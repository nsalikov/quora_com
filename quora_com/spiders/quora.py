# -*- coding: utf-8 -*-
import re
import json
import scrapy
from scrapy.exceptions import CloseSpider


class RelatedSpider(scrapy.Spider):
    name = 'quora'
    allowed_domains = ['quora.com']
    max_answers = None # None, 1, 2, 3, etc

    topics = None
    questions = None

    topics_set = set()
    questions_set = set()


    def start_requests(self):
        if not self.topics:
            self.logger.warning('Unable to find topics file')
            raise CloseSpider('You should specify topics file: scrapy crawl quora -a topics=topics.txt -a questions=questions.jl')

        if not self.questions:
            self.logger.warning('Unable to find questions file')
            raise CloseSpider('You should specify questions file: scrapy crawl quora -a topics=topics.txt -a questions=questions.jl')

        with open(self.topics, 'r', encoding='utf-8') as fin:
            for line in fin:
                topic = line.strip()

                if topic not in self.topics_set:
                    self.topics_set.add(topic)

        with open(self.questions, 'r', encoding='utf-8') as fin:
            for line in fin:
                try:
                    data = json.loads(line)
                    url = data['url']

                    if url not in self.questions_set:
                        self.questions_set.add(url)
                        yield scrapy.Request(url, callback=self.parse)
                except:
                    self.logger.info('Unable to parse json line in init file: {}'.format(line))


    def parse(self, response):
        d = {}

        topics = response.css('span.TopicName ::text').extract()
        topics = [t.strip() for t in topics]
        topics_set = set(topics)

        if not len(topics_set & self.topics_set):
            return

        # if len(topics_set - self.topics_set):
        #     return

        question = response.css('h1 span.ui_story_title span.ui_qtext_rendered_qtext ::text').extract_first()
        answers = response.css('div.pagedlist_item div.Answer div.ExpandedContent div.ui_qtext_expanded').extract()

        for a in answers[:self.max_answers]:
            d['url'] = response.url
            d['topics'] = topics
            d['question'] = question
            d['answer'] = a

            yield d

        related_questions = response.css('div.question_related ul.list_contents li.related_question a.question_link ::attr(href)').extract()
        related_questions = [response.urljoin(url.strip()) for url in related_questions]

        for url in related_questions:
            if url not in self.questions_set:
                self.questions_set.add(url)
                yield response.follow(url, callback=self.parse)
